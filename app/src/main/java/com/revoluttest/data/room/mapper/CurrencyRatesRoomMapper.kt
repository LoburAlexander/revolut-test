package com.revoluttest.data.room.mapper

import com.revoluttest.data.room.entity.CurrencyRateRoom
import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRate
import com.revoluttest.domain.entity.CurrencyRates
import javax.inject.Inject

class CurrencyRatesRoomMapper
@Inject
constructor() {

    fun mapRoomToDomain(
        baseCurrency: Currency,
        currencyRates: List<CurrencyRateRoom>
    ): CurrencyRates {
        return CurrencyRates(
            baseCurrency = baseCurrency,
            rates = mapCurrencyRateList(currencyRates)
        )
    }

    fun mapDomainToRoom(
        currencyRates: CurrencyRates
    ): List<CurrencyRateRoom> {
        return currencyRates.rates.map { currencyRate ->
            CurrencyRateRoom(
                baseCurrencyCode = currencyRates.baseCurrency.currencyCode,
                targetCurrencyCode = currencyRate.targetCurrency.currencyCode,
                convertRate = currencyRate.convertRate
            )
        }
    }

    private fun mapCurrencyRateList(ratesRoom: List<CurrencyRateRoom>): List<CurrencyRate> {
        return ratesRoom.map { rateRoom ->
            CurrencyRate(
                targetCurrency = Currency(rateRoom.targetCurrencyCode),
                convertRate = rateRoom.convertRate
            )
        }
    }
}
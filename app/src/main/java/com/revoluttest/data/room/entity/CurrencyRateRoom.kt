package com.revoluttest.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.revoluttest.data.room.constants.RoomColumns
import com.revoluttest.data.room.constants.RoomTables

@Entity(
    tableName = RoomTables.CURRENCY_RATE,
    primaryKeys = [
        RoomColumns.CurrencyRate.BASE_CURRENCY_CODE,
        RoomColumns.CurrencyRate.TARGET_CURRENCY_CODE
    ]
)
data class CurrencyRateRoom(

    @ColumnInfo(name = RoomColumns.CurrencyRate.BASE_CURRENCY_CODE)
    val baseCurrencyCode: String,

    @ColumnInfo(name = RoomColumns.CurrencyRate.TARGET_CURRENCY_CODE)
    val targetCurrencyCode: String,

    @ColumnInfo(name = RoomColumns.CurrencyRate.CONVERT_RATE)
    val convertRate: Double
)
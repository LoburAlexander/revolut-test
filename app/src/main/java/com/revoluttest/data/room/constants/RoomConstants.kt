package com.revoluttest.data.room.constants

object RoomTables {
    const val CURRENCY_RATE: String = "currency_rate"
}

object RoomColumns {

    object CurrencyRate {
        const val BASE_CURRENCY_CODE: String = "base_currency_code"
        const val TARGET_CURRENCY_CODE: String = "target_currency_code"
        const val CONVERT_RATE: String = "convert_rate"
    }
}
package com.revoluttest.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.revoluttest.data.room.dao.CurrencyRateDao
import com.revoluttest.data.room.entity.CurrencyRateRoom

@Database(entities = [CurrencyRateRoom::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun currencyRateDao(): CurrencyRateDao
}
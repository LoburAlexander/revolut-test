package com.revoluttest.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.revoluttest.data.room.constants.RoomColumns
import com.revoluttest.data.room.constants.RoomTables
import com.revoluttest.data.room.entity.CurrencyRateRoom
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface CurrencyRateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateCurrencyRates(currencyRates: List<CurrencyRateRoom>): Completable

    @Query("SELECT * FROM ${RoomTables.CURRENCY_RATE} WHERE ${RoomColumns.CurrencyRate.BASE_CURRENCY_CODE} = :baseCurrencyCode")
    fun getCurrencyRates(baseCurrencyCode: String): Single<List<CurrencyRateRoom>>

    @Query("SELECT * FROM ${RoomTables.CURRENCY_RATE} WHERE ${RoomColumns.CurrencyRate.BASE_CURRENCY_CODE} = :baseCurrencyCode")
    fun observeCurrencyRatesUpdates(baseCurrencyCode: String): Flowable<List<CurrencyRateRoom>>
}
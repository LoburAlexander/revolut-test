package com.revoluttest.data.repository

import com.revoluttest.data.room.dao.CurrencyRateDao
import com.revoluttest.data.room.mapper.CurrencyRatesRoomMapper
import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRates
import com.revoluttest.domain.repository.CurrencyRateLocalRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RoomCurrencyRateLocalRepository
@Inject
constructor(
    private val currencyRateDao: CurrencyRateDao,
    private val currencyRatesRoomMapper: CurrencyRatesRoomMapper
) : CurrencyRateLocalRepository {

    override fun insertOrUpdateCurrencyRates(currencyRates: CurrencyRates): Completable {
        return Single.fromCallable { currencyRatesRoomMapper.mapDomainToRoom(currencyRates) }
            .flatMapCompletable { currencyRatesRoom ->
                currencyRateDao.insertOrUpdateCurrencyRates(currencyRatesRoom)
            }
    }

    override fun getCurrencyRates(baseCurrency: Currency): Single<CurrencyRates> {
        return currencyRateDao.getCurrencyRates(baseCurrency.currencyCode)
            .map { currencyRateList ->
                currencyRatesRoomMapper.mapRoomToDomain(baseCurrency, currencyRateList)
            }
    }

    override fun observeCurrencyRatesUpdates(baseCurrency: Currency): Flowable<CurrencyRates> {
        return currencyRateDao.observeCurrencyRatesUpdates(baseCurrency.currencyCode)
            .map { currencyRateList ->
                currencyRatesRoomMapper.mapRoomToDomain(baseCurrency, currencyRateList)
            }
    }
}
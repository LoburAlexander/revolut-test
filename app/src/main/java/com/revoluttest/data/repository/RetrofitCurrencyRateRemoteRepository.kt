package com.revoluttest.data.repository

import com.revoluttest.data.retrofit.CurrenciesApi
import com.revoluttest.data.retrofit.mapper.CurrencyRatesRemoteMapper
import com.revoluttest.domain.entity.CurrencyRates
import com.revoluttest.domain.repository.CurrencyRateRemoteRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitCurrencyRateRemoteRepository
@Inject
constructor(
    private val currenciesApi: CurrenciesApi,
    private val currencyRatesRemoteMapper: CurrencyRatesRemoteMapper
) : CurrencyRateRemoteRepository {

    override fun getLatestRates(currencyCode: String?): Single<CurrencyRates> {
        return currenciesApi.getLatestRates(currencyCode)
            .map(currencyRatesRemoteMapper::mapRemoteToDomain)
    }
}
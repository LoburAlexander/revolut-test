package com.revoluttest.data.retrofit

import com.revoluttest.data.retrofit.entity.CurrencyRatesRemote
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrenciesApi {

    @GET("/api/android/latest")
    fun getLatestRates(@Query("base") currencyCode: String? = null): Single<CurrencyRatesRemote>
}
package com.revoluttest.data.retrofit.mapper

import com.revoluttest.data.retrofit.entity.CurrencyRatesRemote
import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRate
import com.revoluttest.domain.entity.CurrencyRates
import java.util.Locale
import javax.inject.Inject

class CurrencyRatesRemoteMapper
@Inject
constructor() {

    fun mapRemoteToDomain(currencyRatesRemote: CurrencyRatesRemote): CurrencyRates {
        return CurrencyRates(
            baseCurrency = Currency(currencyCode = formatCurrencyCode(currencyRatesRemote.baseCurrencyCode)),
            rates = mapCurrencyRateList(currencyRatesRemote.rates)
        )
    }

    private fun mapCurrencyRateList(ratesRemote: Map<String, Double>): List<CurrencyRate> {
        return ratesRemote.map { (targetCurrencyCode, rate) ->
            CurrencyRate(
                targetCurrency = Currency(currencyCode = formatCurrencyCode(targetCurrencyCode)),
                convertRate = rate
            )
        }
    }

    private fun formatCurrencyCode(remoteCurrencyCode: String): String {
        return remoteCurrencyCode.toUpperCase(Locale.US)
    }
}
package com.revoluttest.data.retrofit.entity

import com.google.gson.annotations.SerializedName

data class CurrencyRatesRemote(

    @SerializedName("baseCurrency")
    val baseCurrencyCode: String,

    @SerializedName("rates")
    val rates: Map<String, Double>
)
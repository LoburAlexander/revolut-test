package com.revoluttest.presentation.base

interface BackPressedListener {

    fun onBackPressed()
}
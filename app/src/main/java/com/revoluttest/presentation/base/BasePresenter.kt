package com.revoluttest.presentation.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter
import moxy.MvpView

abstract class BasePresenter<ViewT : MvpView> : MvpPresenter<ViewT>(), BackPressedListener {

    private val destroyDisposable: CompositeDisposable = CompositeDisposable()
    private val detachViewDisposable: CompositeDisposable = CompositeDisposable()
    private val destroyViewDisposable: CompositeDisposable = CompositeDisposable()

    override fun detachView(view: ViewT) {
        super.detachView(view)
        detachViewDisposable.clear()
    }

    override fun destroyView(view: ViewT) {
        super.destroyView(view)
        destroyViewDisposable.clear()
    }

    override fun onDestroy() {
        destroyDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed() = Unit

    protected fun Disposable.untilViewDetach() {
        detachViewDisposable.add(this)
    }

    protected fun Disposable.untilViewDestroy() {
        destroyViewDisposable.add(this)
    }

    protected fun Disposable.untilDestroy() {
        destroyDisposable.add(this)
    }
}
package com.revoluttest.presentation.currencies.mapper

import com.revoluttest.android.resolver.CountryFlagImageResolver
import com.revoluttest.android.resolver.CurrencyInfoResolver
import com.revoluttest.domain.entity.CurrencyRates
import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem
import javax.inject.Inject

class CurrencyWithAmountItemMapper
@Inject
constructor(
    private val currencyInfoResolver: CurrencyInfoResolver,
    private val countryFlagImageResolver: CountryFlagImageResolver
) {

    fun mapItems(
        currencyRates: CurrencyRates,
        baseCurrencyAmount: Double
    ): List<CurrencyWithAmountItem> {
        return arrayListOf<CurrencyWithAmountItem>()
            .also { resultItems ->
                val baseCurrencyItem = mapItem(
                    isBaseCurrency = true,
                    currencyCode = currencyRates.baseCurrency.currencyCode,
                    currencyAmount = baseCurrencyAmount
                )

                resultItems.add(baseCurrencyItem)
                currencyRates.rates.forEach { currencyRate ->
                    resultItems.add(
                        mapItem(
                            isBaseCurrency = false,
                            currencyCode = currencyRate.targetCurrency.currencyCode,
                            currencyAmount = currencyRate.convertRate * baseCurrencyAmount
                        )
                    )
                }
            }
    }

    private fun mapItem(
        isBaseCurrency: Boolean,
        currencyCode: String,
        currencyAmount: Double
    ): CurrencyWithAmountItem {
        return CurrencyWithAmountItem(
            isBaseCurrency = isBaseCurrency,
            currencyCode = currencyCode,
            currencyName = currencyInfoResolver.getCurrencyDisplayName(currencyCode),
            countryFlagImageUrl = countryFlagImageResolver.getCountryFlagImageUrlByCurrencyCode(
                currencyCode
            ),
            amount = currencyAmount
        )
    }
}
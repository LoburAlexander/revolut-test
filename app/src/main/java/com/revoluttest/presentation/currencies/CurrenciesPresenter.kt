package com.revoluttest.presentation.currencies

import com.revoluttest.android.resolver.NetworkInfoResolver
import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRates
import com.revoluttest.domain.repository.CurrencyRateLocalRepository
import com.revoluttest.domain.usecase.SynchronizeLatestCurrencyRatesUseCase
import com.revoluttest.presentation.base.BasePresenter
import com.revoluttest.presentation.currencies.callback.CurrenciesListCallback
import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem
import com.revoluttest.presentation.currencies.mapper.CurrencyWithAmountItemMapper
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class CurrenciesPresenter
@Inject
constructor(
    private val currencyRateLocalRepository: CurrencyRateLocalRepository,
    private val synchronizeLatestCurrencyRates: SynchronizeLatestCurrencyRatesUseCase,
    private val networkInfoResolver: NetworkInfoResolver,
    private val currencyWithAmountItemMapper: CurrencyWithAmountItemMapper
) : BasePresenter<CurrenciesView>(), CurrenciesListCallback {

    private val baseCurrencyChangeDisposable: CompositeDisposable = CompositeDisposable()
    private val baseCurrencyAmountProcessor: BehaviorProcessor<Double> =
        BehaviorProcessor.createDefault(DEFAULT_BASE_CURRENCY_AMOUNT)

    private var currentBaseCurrencyCode: String = DEFAULT_BASE_CURRENCY_CODE
    private var currentBaseCurrencyAmount: Double
        get() = baseCurrencyAmountProcessor.value ?: DEFAULT_BASE_CURRENCY_AMOUNT
        set(value) = baseCurrencyAmountProcessor.onNext(value)

    override fun attachView(view: CurrenciesView?) {
        super.attachView(view)
        setupNetworkConnectionStream()
        setupCurrencyRatesStreams(currentBaseCurrencyCode)
    }

    override fun detachView(view: CurrenciesView) {
        disposeCurrencyRatesStreams()
        super.detachView(view)
    }

    override fun onItemAmountFocused(position: Int, item: CurrencyWithAmountItem) {
        setupCurrentBaseCurrencyIfChanged(item.currencyCode, item.amount)
    }

    override fun onItemAmountChanged(position: Int, item: CurrencyWithAmountItem, amount: Double) {
        if (item.currencyCode == currentBaseCurrencyCode) {
            currentBaseCurrencyAmount = amount
        }
    }

    private fun setupNetworkConnectionStream() {
        networkInfoResolver.observeIsNetworkAvailable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { isNetworkAvailable ->
                if (!isNetworkAvailable) {
                    notifyRatesNotUpdating()
                }
            }
            .untilViewDetach()
    }

    private fun setupCurrentBaseCurrencyIfChanged(
        currencyCode: String,
        currencyAmount: Double
    ) {
        if (currencyCode != currentBaseCurrencyCode) {
            disposeCurrencyRatesStreams()
            currentBaseCurrencyCode = currencyCode
            currentBaseCurrencyAmount = currencyAmount
            setupCurrencyRatesStreams(currencyCode)
        }
    }

    private fun setupCurrencyRatesStreams(baseCurrencyCode: String) {
        disposeCurrencyRatesStreams()

        val isNetworkAvailableFilterFlowable = networkInfoResolver.observeIsNetworkAvailable()
            .filter { isNetworkAvailable -> isNetworkAvailable }

        val currencyRatesUpdateFlowable =
            Flowable.interval(CURRENCY_RATES_UPDATE_INTERVAL_SECONDS, TimeUnit.SECONDS)
                .onBackpressureDrop()
                .flatMap { isNetworkAvailableFilterFlowable }

        synchronizeLatestCurrencyRates.execute(baseCurrencyCode)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                notifyRatesNotUpdating()
            }
            .observeOn(Schedulers.io())
            .retryWhen { errorFlowable ->
                errorFlowable.flatMap { currencyRatesUpdateFlowable.take(1) }
            }
            .repeatWhen { completeFlowable ->
                completeFlowable.flatMap { currencyRatesUpdateFlowable.take(1) }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {},
                { error ->
                    // Shouldn't happen normally
                    Timber.e(error)
                }
            )
            .untilCurrencyRatesStreamsDispose()

        Flowable.combineLatest(
            currencyRateLocalRepository.observeCurrencyRatesUpdates(Currency(baseCurrencyCode))
                .observeOn(Schedulers.io()),
            baseCurrencyAmountProcessor.onBackpressureLatest()
                .observeOn(Schedulers.io()),
            BiFunction<CurrencyRates, Double, List<CurrencyWithAmountItem>> { currencyRates, baseCurrencyAmount ->
                currencyWithAmountItemMapper.mapItems(currencyRates, baseCurrencyAmount)
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { currencyWithAmountItems ->
                    viewState.updateCurrencyWithAmountItems(currencyWithAmountItems)
                },
                { error ->
                    // Shouldn't happen normally
                    Timber.e(error)
                }
            )
            .untilCurrencyRatesStreamsDispose()
    }

    private fun notifyRatesNotUpdating() {
        // Notify user somehow, that rates are not updating
        viewState.showMessage("Failed to update rates")
    }

    private fun disposeCurrencyRatesStreams() {
        baseCurrencyChangeDisposable.clear()
    }

    private fun Disposable.untilCurrencyRatesStreamsDispose() {
        baseCurrencyChangeDisposable.add(this)
    }

    companion object {
        const val DEFAULT_BASE_CURRENCY_CODE: String = "EUR"
        const val DEFAULT_BASE_CURRENCY_AMOUNT: Double = 10.0
        const val CURRENCY_RATES_UPDATE_INTERVAL_SECONDS: Long = 1L
    }
}
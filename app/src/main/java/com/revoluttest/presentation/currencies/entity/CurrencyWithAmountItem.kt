package com.revoluttest.presentation.currencies.entity

data class CurrencyWithAmountItem(
    val isBaseCurrency: Boolean,
    val currencyCode: String,
    val currencyName: String,
    val countryFlagImageUrl: String?,
    val amount: Double
)
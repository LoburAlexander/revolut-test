package com.revoluttest.presentation.currencies.callback

import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem

interface CurrenciesListCallback {

    fun onItemAmountFocused(position: Int, item: CurrencyWithAmountItem)

    fun onItemAmountChanged(position: Int, item: CurrencyWithAmountItem, amount: Double)
}
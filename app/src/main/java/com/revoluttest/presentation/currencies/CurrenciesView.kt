package com.revoluttest.presentation.currencies

import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface CurrenciesView : MvpView {

    fun updateCurrencyWithAmountItems(items: List<CurrencyWithAmountItem>)

    @StateStrategyType(SkipStrategy::class)
    fun showMessage(message: String)
}
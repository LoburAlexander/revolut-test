package com.revoluttest.domain.entity

data class CurrencyRates(
    val baseCurrency: Currency,
    val rates: List<CurrencyRate>
)
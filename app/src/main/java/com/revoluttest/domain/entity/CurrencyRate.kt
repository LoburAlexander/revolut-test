package com.revoluttest.domain.entity

data class CurrencyRate(
    val targetCurrency: Currency,
    val convertRate: Double
)
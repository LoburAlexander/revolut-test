package com.revoluttest.domain.entity

data class Currency(
    val currencyCode: String
)
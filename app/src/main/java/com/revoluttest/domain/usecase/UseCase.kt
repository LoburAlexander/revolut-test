package com.revoluttest.domain.usecase

interface UseCase<InputT : Any, OutputT : Any> {

    fun execute(input: InputT): OutputT
}
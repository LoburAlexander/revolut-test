package com.revoluttest.domain.usecase

import com.revoluttest.domain.repository.CurrencyRateLocalRepository
import com.revoluttest.domain.repository.CurrencyRateRemoteRepository
import io.reactivex.Completable
import javax.inject.Inject

class SynchronizeLatestCurrencyRatesUseCase
@Inject
constructor(
    private val currencyRateRemoteRepository: CurrencyRateRemoteRepository,
    private val currencyRateLocalRepository: CurrencyRateLocalRepository
) : UseCase<String, Completable> {

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun execute(baseCurrencyCode: String): Completable {
        return currencyRateRemoteRepository.getLatestRates(baseCurrencyCode)
            .flatMapCompletable { remoteCurrencyRates ->
                currencyRateLocalRepository.insertOrUpdateCurrencyRates(remoteCurrencyRates)
            }
    }
}
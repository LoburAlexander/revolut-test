package com.revoluttest.domain.repository

import com.revoluttest.domain.entity.CurrencyRates
import io.reactivex.Single

interface CurrencyRateRemoteRepository {

    fun getLatestRates(currencyCode: String? = null): Single<CurrencyRates>
}
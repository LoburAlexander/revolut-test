package com.revoluttest.domain.repository

import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRates
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface CurrencyRateLocalRepository {

    fun insertOrUpdateCurrencyRates(currencyRates: CurrencyRates): Completable

    fun getCurrencyRates(baseCurrency: Currency): Single<CurrencyRates>

    fun observeCurrencyRatesUpdates(baseCurrency: Currency): Flowable<CurrencyRates>
}
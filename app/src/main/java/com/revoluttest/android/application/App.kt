package com.revoluttest.android.application

import android.app.Application
import com.revoluttest.BuildConfig
import com.revoluttest.di.Injector
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initDependencyInjection()
        initLogger()
    }

    private fun initDependencyInjection() {
        Injector.init(this)
        Injector.getApplicationComponent().inject(this)
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
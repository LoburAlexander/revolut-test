package com.revoluttest.android.resolver

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.core.net.ConnectivityManagerCompat
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkInfoResolver
@Inject
constructor(
    context: Context
) {

    private val isNetworkAvailableProcessor: BehaviorProcessor<Boolean> = BehaviorProcessor.create()

    init {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connectivityManager.registerNetworkCallback(buildNetworkRequest(), NetworkCallback())
        isNetworkAvailableProcessor.onNext(connectivityManager.activeNetworkInfo != null)
    }

    fun observeIsNetworkAvailable(): Flowable<Boolean> {
        return isNetworkAvailableProcessor
    }

    private fun buildNetworkRequest(): NetworkRequest {
        return NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()
    }

    private inner class NetworkCallback : ConnectivityManager.NetworkCallback() {

        override fun onAvailable(network: Network?) {
            isNetworkAvailableProcessor.onNext(true)
        }

        override fun onLost(network: Network?) {
            isNetworkAvailableProcessor.onNext(false)
        }
    }
}
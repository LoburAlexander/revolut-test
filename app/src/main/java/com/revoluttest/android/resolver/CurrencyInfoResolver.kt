package com.revoluttest.android.resolver

import android.content.res.Resources
import com.revoluttest.R
import java.util.Currency
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyInfoResolver
@Inject
constructor(
    private val resources: Resources
) {

    fun getCurrencyCountryCode(currencyCode: String): String {
        return currencyCode.substring(0, 2)
    }

    fun getCurrencyFractionDigits(currencyCode: String): Int {
        return try {
            Currency.getInstance(currencyCode).defaultFractionDigits
                .let { if (it >= 0) it else MAX_CURRENCY_FRACTION_DIGITS_COUNT }
        } catch (e: Throwable) {
            MAX_CURRENCY_FRACTION_DIGITS_COUNT
        }
    }

    fun getCurrencyDisplayName(currencyCode: String): String {
        return try {
            Currency.getInstance(currencyCode).displayName
        } catch (e: Throwable) {
            resources.getString(R.string.currency_display_name_unknown)
        }
    }

    companion object {
        const val MAX_CURRENCY_FRACTION_DIGITS_COUNT = 4
    }
}
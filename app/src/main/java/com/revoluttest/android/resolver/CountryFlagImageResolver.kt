package com.revoluttest.android.resolver

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CountryFlagImageResolver
@Inject
constructor(
    private val currencyInfoResolver: CurrencyInfoResolver
) {

    fun getCountryFlagImageUrlByCurrencyCode(currencyCode: String): String? {
        val countryCode = currencyInfoResolver.getCurrencyCountryCode(currencyCode)
        return COUNTRY_FLAG_IMAGE_URL_PATTERN.format(countryCode)
    }

    companion object {
        const val COUNTRY_FLAG_IMAGE_URL_PATTERN = "https://www.countryflags.io/%s/flat/64.png"
    }
}
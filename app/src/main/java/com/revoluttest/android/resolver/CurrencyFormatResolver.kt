package com.revoluttest.android.resolver

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyFormatResolver
@Inject
constructor(
    private val currencyInfoResolver: CurrencyInfoResolver
) {

    val currencyFractionSeparator: Char
        get() = decimalFormatSymbols.decimalSeparator

    private val currencyAmountFormatterCache: MutableMap<String, DecimalFormat> = hashMapOf()
    private val decimalFormatSymbols: DecimalFormatSymbols =
        DecimalFormatSymbols.getInstance(Locale.getDefault())

    fun formatCurrencyAmount(currencyCode: String, amount: Double): String {
        return getAmountFormatterForCurrency(currencyCode).format(amount)
            .let { formattedAmount ->
                if (formattedAmount.endsWith(currencyFractionSeparator)) {
                    formattedAmount.removeSuffix(currencyFractionSeparator.toString())
                } else {
                    formattedAmount
                }
            }
    }

    fun parseCurrencyAmount(currencyCode: String, amountText: String): Double {
        return try {
            getAmountFormatterForCurrency(currencyCode).parse(amountText).toDouble()
        } catch (error: Throwable) {
            0.0
        }
    }

    private fun getAmountFormatterForCurrency(currencyCode: String): DecimalFormat {
        return currencyAmountFormatterCache[currencyCode]
            ?: buildAmountFormatterForCurrency(currencyCode).also { formatter ->
                currencyAmountFormatterCache[currencyCode] = formatter
            }
    }

    private fun buildAmountFormatterForCurrency(currencyCode: String): DecimalFormat {
        val fractionDigitsCount = currencyInfoResolver.getCurrencyFractionDigits(currencyCode)
        val fractionPattern = "#".repeat(fractionDigitsCount)
        val pattern = "#,###.$fractionPattern"
        return DecimalFormat(pattern, decimalFormatSymbols)
    }
}
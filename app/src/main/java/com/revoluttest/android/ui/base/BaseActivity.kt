package com.revoluttest.android.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.revoluttest.presentation.base.BasePresenter
import moxy.MvpAppCompatActivity
import moxy.MvpView
import javax.inject.Inject
import javax.inject.Provider

abstract class BaseActivity<PresenterT : BasePresenter<out MvpView>> : MvpAppCompatActivity() {

    @Inject
    lateinit var presenterProvider: Provider<PresenterT>

    protected abstract val layoutId: Int @LayoutRes get

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
    }

    protected abstract fun injectDependencies()
}
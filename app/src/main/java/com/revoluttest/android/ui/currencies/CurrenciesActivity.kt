package com.revoluttest.android.ui.currencies

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.revoluttest.R
import com.revoluttest.android.adapter.currencies.CurrenciesAdapter
import com.revoluttest.android.resolver.CurrencyFormatResolver
import com.revoluttest.android.ui.base.BaseActivity
import com.revoluttest.di.Injector
import com.revoluttest.presentation.currencies.CurrenciesPresenter
import com.revoluttest.presentation.currencies.CurrenciesView
import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem
import kotlinx.android.synthetic.main.activity_main.vRecyclerCurrencies
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class CurrenciesActivity : BaseActivity<CurrenciesPresenter>(), CurrenciesView {

    override val layoutId: Int = R.layout.activity_main

    @Inject
    lateinit var currencyFormatResolver: CurrencyFormatResolver

    @InjectPresenter
    lateinit var presenter: CurrenciesPresenter
    @ProvidePresenter
    fun providePresenter(): CurrenciesPresenter = presenterProvider.get()

    override fun injectDependencies() {
        Injector.newCurrenciesSubComponent()
            .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vRecyclerCurrencies.apply {
            if (itemAnimator is SimpleItemAnimator) {
                (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            }
            layoutManager = LinearLayoutManager(context)
            adapter = CurrenciesAdapter(
                currencyFormatResolver = currencyFormatResolver,
                callback = presenter
            ).also { adapter ->
                adapter.setHasStableIds(true)
            }
        }
    }

    override fun updateCurrencyWithAmountItems(items: List<CurrencyWithAmountItem>) {
        (vRecyclerCurrencies.adapter as? CurrenciesAdapter)?.submitList(items)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}

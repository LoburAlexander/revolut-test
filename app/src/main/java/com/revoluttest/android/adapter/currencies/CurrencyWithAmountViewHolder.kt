package com.revoluttest.android.adapter.currencies

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.revoluttest.R
import com.revoluttest.android.adapter.base.BaseViewHolder
import com.revoluttest.android.resolver.CurrencyFormatResolver
import com.revoluttest.presentation.currencies.callback.CurrenciesListCallback
import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem

class CurrencyWithAmountViewHolder(
    parent: ViewGroup,
    private val currencyFormatResolver: CurrencyFormatResolver,
    private val callback: CurrenciesListCallback
) : BaseViewHolder<CurrencyWithAmountItem>(parent, R.layout.item_currency_with_amount) {

    private val glide = Glide.with(parent)
    private val rootView: ViewGroup = itemView.findViewById(R.id.vClRoot)
    private val imageCountryFlag: ImageView = itemView.findViewById(R.id.vIvCountryFlag)
    private val textCurrencyCode: TextView = itemView.findViewById(R.id.vTvCurrencyCode)
    private val textCurrencyName: TextView = itemView.findViewById(R.id.vTvCurrencyName)
    private val editTextAmount: EditText = itemView.findViewById(R.id.vEtAmount)
    private val amountTextWatcher: TextWatcher = AmountTextWatcher(editTextAmount)
    private val amountFocusChangeListener: AmountFocusChangeListener = AmountFocusChangeListener()
    private val inputMethodManager: InputMethodManager =
        parent.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    private var currentPosition: Int = 0
    private lateinit var currentItem: CurrencyWithAmountItem

    override fun bind(item: CurrencyWithAmountItem) {
        currentPosition = adapterPosition
        currentItem = item

        bindCountryFlagImage(item)
        bindCurrencyInfoTexts(item)
        bindCurrencyAmountEditText(item)

        rootView.setOnClickListener {
            focusAmountEditTextWithSoftInput()
        }
    }

    override fun onViewAttachedToWindow() {
        super.onViewAttachedToWindow()
        if (currentItem.isBaseCurrency) {
            focusAmountEditTextWithSoftInput()
        }
        setupAmountEditTextListeners()
    }

    override fun onViewDetachedFromWindow() {
        super.onViewDetachedFromWindow()
        clearAmountEditTextListeners()
        editTextAmount.clearFocus()
    }

    private fun bindCountryFlagImage(item: CurrencyWithAmountItem) {
        glide.load(item.countryFlagImageUrl)
            .placeholder(R.drawable.country_flag_placeholder)
            .error(R.drawable.country_flag_placeholder)
            .circleCrop()
            .into(imageCountryFlag)
    }

    private fun bindCurrencyInfoTexts(item: CurrencyWithAmountItem) {
        textCurrencyCode.text = item.currencyCode
        textCurrencyName.text = item.currencyName
    }

    private fun bindCurrencyAmountEditText(item: CurrencyWithAmountItem) {
        val newAmountString =
            currencyFormatResolver.formatCurrencyAmount(item.currencyCode, item.amount)

        if (editTextAmount.text.toString() != newAmountString) {
            editTextAmount.setText(newAmountString)
            editTextAmount.setSelection(editTextAmount.text.length)
        }
    }

    private fun setupAmountEditTextListeners() {
        editTextAmount.onFocusChangeListener = amountFocusChangeListener
        editTextAmount.addTextChangedListener(amountTextWatcher)
    }

    private fun clearAmountEditTextListeners() {
        editTextAmount.removeTextChangedListener(amountTextWatcher)
        editTextAmount.onFocusChangeListener = null
    }

    private fun focusAmountEditTextWithSoftInput() {
        if (!editTextAmount.isFocused) {
            editTextAmount.requestFocus()
        }
        inputMethodManager.showSoftInput(editTextAmount, InputMethodManager.SHOW_IMPLICIT)
    }

    private inner class AmountFocusChangeListener : View.OnFocusChangeListener {

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            if (hasFocus) {
                callback.onItemAmountFocused(currentPosition, currentItem)
            }
        }
    }

    private inner class AmountTextWatcher(
        private val editText: EditText
    ) : TextWatcher {

        override fun afterTextChanged(text: Editable?) {
            editText.removeTextChangedListener(this)

            val currentText = text.toString()
            val currentDoubleValue = currencyFormatResolver.parseCurrencyAmount(
                currentItem.currencyCode,
                currentText
            )
            var formattedText: String = currencyFormatResolver.formatCurrencyAmount(
                currentItem.currencyCode,
                currentDoubleValue
            )

            if (currentText.endsWith(currencyFormatResolver.currencyFractionSeparator)) {
                formattedText += currencyFormatResolver.currencyFractionSeparator
            }

            editText.setText(formattedText)
            editText.setSelection(editText.text.length)

            editText.addTextChangedListener(this)

            callback.onItemAmountChanged(currentPosition, currentItem, currentDoubleValue)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) = Unit
    }
}
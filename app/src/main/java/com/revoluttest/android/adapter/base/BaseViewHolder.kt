package com.revoluttest.android.adapter.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<ItemT>(
    parent: ViewGroup,
    @LayoutRes
    layoutId: Int
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
) {

    abstract fun bind(item: ItemT)

    open fun onViewAttachedToWindow() = Unit

    open fun onViewDetachedFromWindow() = Unit
}
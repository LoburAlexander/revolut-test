package com.revoluttest.android.adapter.currencies

import androidx.recyclerview.widget.DiffUtil
import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem

class CurrenciesDiffUtilItemCallback : DiffUtil.ItemCallback<CurrencyWithAmountItem>() {

    override fun areItemsTheSame(
        oldItem: CurrencyWithAmountItem,
        newItem: CurrencyWithAmountItem
    ): Boolean {
        return oldItem.currencyCode == newItem.currencyCode
    }

    override fun areContentsTheSame(
        oldItem: CurrencyWithAmountItem,
        newItem: CurrencyWithAmountItem
    ): Boolean {
        return oldItem.amount == newItem.amount
    }
}
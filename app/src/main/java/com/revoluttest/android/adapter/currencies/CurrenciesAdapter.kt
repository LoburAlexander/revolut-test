package com.revoluttest.android.adapter.currencies

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.revoluttest.android.resolver.CurrencyFormatResolver
import com.revoluttest.presentation.currencies.callback.CurrenciesListCallback
import com.revoluttest.presentation.currencies.entity.CurrencyWithAmountItem

class CurrenciesAdapter(
    private val currencyFormatResolver: CurrencyFormatResolver,
    private val callback: CurrenciesListCallback
) : ListAdapter<CurrencyWithAmountItem, CurrencyWithAmountViewHolder>(
    CurrenciesDiffUtilItemCallback()
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyWithAmountViewHolder {
        return CurrencyWithAmountViewHolder(parent, currencyFormatResolver, callback)
    }

    override fun onBindViewHolder(holder: CurrencyWithAmountViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onViewAttachedToWindow(holder: CurrencyWithAmountViewHolder) {
        holder.onViewAttachedToWindow()
    }

    override fun onViewDetachedFromWindow(holder: CurrencyWithAmountViewHolder) {
        holder.onViewDetachedFromWindow()
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).currencyCode.hashCode().toLong()
    }
}
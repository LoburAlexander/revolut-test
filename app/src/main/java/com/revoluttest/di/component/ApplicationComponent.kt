package com.revoluttest.di.component

import android.app.Application
import com.revoluttest.android.application.App
import com.revoluttest.di.module.AndroidModule
import com.revoluttest.di.module.RepositoryModule
import com.revoluttest.di.module.RetrofitModule
import com.revoluttest.di.module.RoomModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidModule::class,
        RepositoryModule::class,
        RetrofitModule::class,
        RoomModule::class
    ]
)
interface ApplicationComponent {

    fun inject(app: App)

    fun newCurrenciesSubComponent(): CurrenciesSubComponent

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }
}
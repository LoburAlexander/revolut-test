package com.revoluttest.di.component

import com.revoluttest.android.ui.currencies.CurrenciesActivity
import dagger.Subcomponent

@Subcomponent
interface CurrenciesSubComponent {

    fun inject(activity: CurrenciesActivity)
}
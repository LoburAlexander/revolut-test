package com.revoluttest.di

import com.revoluttest.android.application.App
import com.revoluttest.di.component.ApplicationComponent
import com.revoluttest.di.component.CurrenciesSubComponent
import com.revoluttest.di.component.DaggerApplicationComponent

class Injector {

    companion object {

        private lateinit var sApplicationComponent: ApplicationComponent

        fun init(app: App) {
            sApplicationComponent = DaggerApplicationComponent.builder()
                .application(app)
                .build()
        }

        fun getApplicationComponent(): ApplicationComponent {
            return sApplicationComponent
        }

        fun newCurrenciesSubComponent(): CurrenciesSubComponent {
            return sApplicationComponent.newCurrenciesSubComponent()
        }
    }
}
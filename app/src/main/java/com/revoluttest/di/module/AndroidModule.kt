package com.revoluttest.di.module

import android.app.Application
import android.content.Context
import android.content.res.Resources
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [AndroidModule.Bindings::class])
class AndroidModule {

    @Provides
    fun provideResources(context: Context): Resources =
        context.resources

    @Module
    interface Bindings {

        @Binds
        fun bindContext(app: Application): Context
    }
}
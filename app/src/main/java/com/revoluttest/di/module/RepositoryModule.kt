package com.revoluttest.di.module

import com.revoluttest.data.repository.RetrofitCurrencyRateRemoteRepository
import com.revoluttest.data.repository.RoomCurrencyRateLocalRepository
import com.revoluttest.domain.repository.CurrencyRateLocalRepository
import com.revoluttest.domain.repository.CurrencyRateRemoteRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindCurrenciesRemoteRepository(repository: RetrofitCurrencyRateRemoteRepository): CurrencyRateRemoteRepository

    @Binds
    fun bindCurrenciesLocalRepository(repository: RoomCurrencyRateLocalRepository): CurrencyRateLocalRepository
}

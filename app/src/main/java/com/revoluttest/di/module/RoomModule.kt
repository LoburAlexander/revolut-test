package com.revoluttest.di.module

import android.content.Context
import androidx.room.Room
import com.revoluttest.data.room.AppDatabase
import com.revoluttest.data.room.dao.CurrencyRateDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideRoomDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            DATABASE_NAME
        ).build()
    }

    @Singleton
    @Provides
    fun provideCurrencyRateDao(appDatabase: AppDatabase): CurrencyRateDao {
        return appDatabase.currencyRateDao()
    }

    companion object {
        private const val DATABASE_NAME: String = "app_database"
    }
}
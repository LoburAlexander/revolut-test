package com.revoluttest.usecase

import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRates
import com.revoluttest.domain.repository.CurrencyRateLocalRepository
import com.revoluttest.domain.repository.CurrencyRateRemoteRepository
import com.revoluttest.domain.usecase.SynchronizeLatestCurrencyRatesUseCase
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test
import java.io.IOException

class SynchronizeLatestCurrencyRatesTests {

    private val currencyRateRemoteRepository = mockk<CurrencyRateRemoteRepository>()
    private val currencyRateLocalRepository = mockk<CurrencyRateLocalRepository>()
    private val synchronizeLatestCurrencyRatesUseCase =
        SynchronizeLatestCurrencyRatesUseCase(
            currencyRateRemoteRepository,
            currencyRateLocalRepository
        )

    @Test
    fun `WHEN remote repository return success and local repository return success THEN complete`() {
        val baseCurrencyCode = "EUR"
        val domainModel = CurrencyRates(Currency(baseCurrencyCode), emptyList())

        every { currencyRateRemoteRepository.getLatestRates(any()) } returns Single.just(domainModel)
        every { currencyRateLocalRepository.insertOrUpdateCurrencyRates(any()) } returns Completable.complete()

        val testObserver = synchronizeLatestCurrencyRatesUseCase.execute(baseCurrencyCode).test()

        testObserver
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    fun `WHEN remote repository return success and local repository return error THEN return error`() {
        val baseCurrencyCode = "EUR"
        val domainModel = CurrencyRates(Currency(baseCurrencyCode), emptyList())

        every { currencyRateRemoteRepository.getLatestRates(any()) } returns Single.just(domainModel)
        every { currencyRateLocalRepository.insertOrUpdateCurrencyRates(any()) } returns
                Completable.error(IOException())

        val testObserver = synchronizeLatestCurrencyRatesUseCase.execute(baseCurrencyCode).test()

        testObserver
            .assertNotComplete()
            .assertError(IOException::class.java)
    }

    @Test
    fun `WHEN remote repository return error THEN return error`() {
        val baseCurrencyCode = "EUR"

        every { currencyRateRemoteRepository.getLatestRates(any()) } returns
                Single.error(IOException())

        val testObserver = synchronizeLatestCurrencyRatesUseCase.execute(baseCurrencyCode).test()

        testObserver
            .assertNotComplete()
            .assertError(IOException::class.java)
    }
}

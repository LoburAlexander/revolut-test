package com.revoluttest.repository.remote

import com.revoluttest.data.retrofit.entity.CurrencyRatesRemote
import com.revoluttest.data.retrofit.mapper.CurrencyRatesRemoteMapper
import org.junit.Test

class CurrencyRatesRemoteMapperTests {

    private val currencyRatesRemoteMapper = CurrencyRatesRemoteMapper()

    @Test
    fun `map not empty rates list`() {
        val baseCurrencyCode = "EUR"
        val targetCurrencyCode = "USD"
        val rate = 1.0

        val remoteModel = CurrencyRatesRemote(
            baseCurrencyCode = baseCurrencyCode,
            rates = mapOf(targetCurrencyCode to rate)
        )

        val domainModel = currencyRatesRemoteMapper.mapRemoteToDomain(remoteModel)

        assert(domainModel.baseCurrency.currencyCode == baseCurrencyCode)
        assert(domainModel.rates.size == 1)
        assert(domainModel.rates.first().targetCurrency.currencyCode == targetCurrencyCode)
        assert(domainModel.rates.first().convertRate == rate)
    }

    @Test
    fun `map empty rates list`() {
        val baseCurrencyCode = "EUR"

        val remoteModel = CurrencyRatesRemote(
            baseCurrencyCode = baseCurrencyCode,
            rates = emptyMap()
        )

        val domainModel = currencyRatesRemoteMapper.mapRemoteToDomain(remoteModel)

        assert(domainModel.baseCurrency.currencyCode == baseCurrencyCode)
        assert(domainModel.rates.isEmpty())
    }
}

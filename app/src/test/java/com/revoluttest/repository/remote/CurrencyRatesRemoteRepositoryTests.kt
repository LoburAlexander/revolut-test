package com.revoluttest.repository.remote

import com.revoluttest.data.repository.RetrofitCurrencyRateRemoteRepository
import com.revoluttest.data.retrofit.CurrenciesApi
import com.revoluttest.data.retrofit.entity.CurrencyRatesRemote
import com.revoluttest.data.retrofit.mapper.CurrencyRatesRemoteMapper
import com.revoluttest.domain.entity.Currency
import com.revoluttest.domain.entity.CurrencyRates
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test
import java.io.IOException

class CurrencyRatesRemoteRepositoryTests {

    private val currenciesApi = mockk<CurrenciesApi>()
    private val currencyRatesRemoteMapper = mockk<CurrencyRatesRemoteMapper>()
    private val currencyRateRemoteRepository =
        RetrofitCurrencyRateRemoteRepository(currenciesApi, currencyRatesRemoteMapper)

    @Test
    fun `WHEN api call successful THEN return success`() {
        val baseCurrencyCode = "EUR"
        val remoteModel = CurrencyRatesRemote(baseCurrencyCode, emptyMap())
        val domainModel = CurrencyRates(Currency(baseCurrencyCode), emptyList())

        every { currenciesApi.getLatestRates(any()) } returns Single.just(remoteModel)
        every { currencyRatesRemoteMapper.mapRemoteToDomain(any()) } returns domainModel

        val testObserver = currencyRateRemoteRepository.getLatestRates(baseCurrencyCode).test()

        testObserver
            .assertValueCount(1)
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    fun `WHEN api call error THEN return error`() {
        val baseCurrencyCode = "EUR"

        every { currenciesApi.getLatestRates(any()) } returns Single.error(IOException())

        val testObserver = currencyRateRemoteRepository.getLatestRates(baseCurrencyCode).test()

        testObserver
            .assertNoValues()
            .assertError(IOException::class.java)
    }
}
